# Generated by Django 3.1.7 on 2021-04-13 08:33

from django.db import migrations
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0002_auto_20210413_0331'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phonenumber',
            name='contact_phone',
            field=phonenumber_field.modelfields.PhoneNumberField(blank=True, help_text='Контактный телефон', max_length=128, region=None),
        ),
        migrations.AlterField(
            model_name='phonenumber',
            name='msisdn',
            field=phonenumber_field.modelfields.PhoneNumberField(blank=True, help_text='Номер телефона', max_length=128, region=None),
        ),
    ]
